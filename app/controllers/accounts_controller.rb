class AccountsController < ApplicationController

def index
    @Accounts = Account.all
    render json: @Accounts.as_json(root: true)
end

def show
	account = Account.find(params[:id])
	return head :not_found unless account
	#render json: account.as_json()
	#render json: Transaction.where(account_id: account.id).as_json()
	statement = [account,Transaction.where(account_id: account.id)]
	render json: statement.as_json(root: true)
end


def create
    return head :unprocessable_entity unless Account.open(account_params)
    render status: :created
  end

def deposit
    account = Account.lock(true).find(params[:id])
    return head :not_found unless account
    #return head :unprocessable_entity unless Account.deposit(account, amount)
    Account.deposit(account, amount)
    Transaction.open(account.id, amount)
    render json: {deposited: true}
 end

 def restart
 	account = Account.find(params[:id])
    return head :not_found unless account
    return head :unprocessable_entity unless Account.restart(account)
    Transaction.where(account_id: account.id).delete_all
    render json: {restarted: true}
 end

private
   def account_params
    params.require(:account).permit(:balance)
  end

    def amount
    param = params.permit(:amount)
    param[:amount].to_i
  	end
end
