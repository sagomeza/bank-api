class Account < ApplicationRecord

	has_many :transactions

  def self.open(params)
    account = new(params)
    puts "Creating a account with #{account.attributes}"
    account.save!
  end

  def self.deposit(account, amount)
    puts "Depositing #{amount} on account #{account.id}"
    #return false unless self.amount_valid?(amount)
    account.balance = (account.balance += amount)
    #account.Transactions.new(amount)
    account.save!
  end

  def self.restart(account)
  	account.balance =0
  	account.save!
  end

private
  def self.amount_valid?(amount)
    if amount <= 0
      puts 'Transaction failed! Amount must be greater than 0.00'
      return false
    end
    return true
  end
end
