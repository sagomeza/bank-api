class Transaction < ApplicationRecord
	belongs_to :account

	def self.open(id, amount)
    transaction = new(account_id: id, amount: amount )
    puts "Creating a transaction with #{transaction.attributes}"
    transaction.save!
  end
end
