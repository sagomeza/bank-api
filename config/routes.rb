Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :accounts do 
  	member do
      post :deposit
      post :restart
    end
  end
end
